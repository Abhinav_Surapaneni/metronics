// Angular
import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'kt-user-profile',
	templateUrl: './user-profile.component.html',
})
export class UserProfileComponent implements OnInit {
	// Public properties
	@Input() avatar: boolean;
	@Input() greeting: boolean;
	@Input() badge: boolean;
	@Input() icon: boolean;
	@Input() user: any;
	@Input() profileImageUrl: string;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor() {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.user = {
			fullname: "Test"
		}
	}

	/**
	 * Log out
	 */
	logout() {

	}
}
